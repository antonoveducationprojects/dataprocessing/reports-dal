using Microsoft.EntityFrameworkCore;
using MySql.EntityFrameworkCore.Extensions;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;

using  ReportsDAL.Models;

namespace ReportsDAL
{

public class MysqlEntityFrameworkDesignTimeServices : IDesignTimeServices
{
    public void ConfigureDesignTimeServices(IServiceCollection serviceCollection)
    {
        serviceCollection.AddEntityFrameworkMySQL();
        new EntityFrameworkRelationalDesignServicesBuilder(serviceCollection)
            .TryAddCoreServices();
    }
}
public class CourseStorageContext: DbContext
{
    
    public DbSet<CourseStartType> CourseStartTypes { get; set; }
    public DbSet<DataSetTable> DataSetTables { get; set; }
    public DbSet<EducationCourse> EducationCourses { get; set; }
    public DbSet<EducationCourseResult> EducationCourseResults { get; set; }
    public DbSet<EducationGroup> EducationGroups { get; set; }
    public DbSet<Location> Locations { get; set; }
    public DbSet<MeetingParticipant> MeetingParticipants { get; set; }
    public DbSet<MeetingSummary> MeetingSummaries { get; set; }
    public DbSet<Participant> Participants { get; set; }    
    public DbSet<Region> Regions { get; set; }
    public DbSet<StudentState> StudentStates { get; set; }    
    public DbSet<Submodule> Submodules { get; set; }
    public DbSet<TaskMinLevel> TaskMinLevels { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseMySQL("server=localhost;database=coursestorage;user=dotnet;password=12345;CharSet=utf8;");
    }
}
}