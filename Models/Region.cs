using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ReportsDAL.Models
{
    public class Region
    {
        [Key]
        public ulong RegionId{ get; set; }
        [MaxLength(100)]
        [Required]
        public string RegionName{ get; set; }
        public ulong LocationId{ get; set; }
        public Location Location { get; set; }
    
        public virtual ICollection<Participant> Participants { get; set; }
    } 
}