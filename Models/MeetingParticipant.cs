using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using  System;

namespace ReportsDAL.Models
{
    public class MeetingParticipant
    {
        public ulong MeetingParticipantId { get; set; }
        [MaxLength(100)]
        [Required]
        public string FullName { get; set; }
        public DateTime JoinTime { get; set; }
        public DateTime LeaveTime { get; set; }
        public DateTime Duration { get; set; }
        [MaxLength(70)]
        [Required]
        public string Email{ get; set; }
        [MaxLength(70)]
        [Required]
        public string Role { get; set; }
        public string ParticipantGUID { get; set; }

        public ulong MeetingSummaryId { get; set; }
        public MeetingSummary MeetingSummary { get; set; }
    };
}