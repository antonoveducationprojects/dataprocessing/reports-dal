using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReportsDAL.Models
{
    public class EducationCourse
    {
        public ulong EducationCourseId{ get; set; }
        [MaxLength(70)]
        [Required]
        public string EducationCourseName{ get; set; }
   
        [ForeignKey("EducationCourseId")]
        public virtual ICollection<EducationCourseResult> EducationCourseResults { get; set; }
        public virtual ICollection<Submodule> Submodules { get; set; }
    }
}