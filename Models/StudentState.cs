using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReportsDAL.Models
{
    public class StudentState
    {
        public ulong StudentStateId{ get; set; }
        [MaxLength(70)]
        [Required]
        public string StateName{ get; set; }
    
        [ForeignKey("StudentStateId")]
        public virtual ICollection<Participant> Participants { get; set; }
    }
}