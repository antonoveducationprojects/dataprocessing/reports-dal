using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReportsDAL.Models
{
    public class MeetingSummary
    {
        [Key]
        public ulong MeetingSummaryId  { get; set; }
        [Required]
        public ulong EducationGroupId{ get; set; }
        public EducationGroup EducationGroup { get; set; }
        [Required]
        public uint  ParticipantsTotalNumber { get; set; }
        [MaxLength(120)]
//    [Required]
        public string MeetingTitle { get; set; }
        [Required]
        public DateTime MeetingStartTime { get; set; }
        [Required]
        public DateTime MeetingEndTime  { get; set; }
        public string MeetingGUID { get; set; }
        public string LectorEmail { get; set; }
        public ulong LectorId { get; set; }
        
        [ForeignKey("MeetingSummaryId")]
        public virtual ICollection<MeetingParticipant> MeetingParticipants { get; set; }
    }
}