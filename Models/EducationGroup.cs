using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReportsDAL.Models
{
    public class EducationGroup
    {
        public ulong EducationGroupId{ get; set; }
        [MaxLength(70)]
        [Required]
        public string EducationGroupName{ get; set; }
        public ulong? CourseStartTypeId{ get; set; }
        public CourseStartType? CourseStartType { get; set; }
        public DateTime StartedAt { get; set; }
        [ForeignKey("EducationGroupId")]
        public virtual ICollection<MeetingSummary> MeetingSummaries { get; set; }
        public virtual ICollection<Participant> Participants { get; set; }
        public virtual ICollection<DataSetTable> DataSetTables { get; set; }
    }
}