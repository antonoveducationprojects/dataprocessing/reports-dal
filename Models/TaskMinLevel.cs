using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ReportsDAL.Models
{
    public class TaskMinLevel
    {
        public ulong TaskMinLevelId { get; set; }
        [MaxLength(150)]
        [Required]
        public string TaskName { get; set; }
        [Required]
        public uint Score { get; set; }
        [MaxLength(100)]
        [Required]
        public string Submodule { get; set; }
        public ulong? SubmoduleId{ get; set; }
    }
}