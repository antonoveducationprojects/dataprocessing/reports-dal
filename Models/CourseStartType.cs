using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ReportsDAL.Models
{
    public class CourseStartType
    {
        public ulong CourseStartTypeId{ get; set; }
        [MaxLength(70)]
        [Required]
        public string StartTypeName{ get; set; }
    
        public virtual ICollection<EducationGroup> EducationGroups { get; set; }
    
    }
}