using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReportsDAL.Models
{
//    [Table("participant")]
    public class Participant
    {
//       [Column("participant_id")]
        public ulong ParticipantId { get; set; }
        
        [Required]
//        [Column("education_course_id")]
        public ulong EducationGroupId{ get; set; }
        public EducationGroup EducationGroup { get; set; }
        public ulong? RegionId { get; set; }
        public Region? Region { get; set; }

        [MaxLength(100)]
    //    [Required]
        public string? RegionName { get; set; }
        [MaxLength(70)]
        public string? GitNick { get; set; }
        [MaxLength(70)]
        [Required]
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        [MaxLength(70)]
  //      [Required]
        public string? EnglishLevel { get; set; }
        [MaxLength(70)]
//        [Required]
        public ulong? StudentStateId { get; set; }
        public StudentState? StudentState { get; set; }
        [MaxLength(250)]
        public string? Comments { get; set; }
        [MaxLength(250)]
        public string? FinalTaskName { get; set; }
        [MaxLength(250)]
        public string? FinalTaskLink { get; set; }
//    public long CourseStartType_id { get; set; }
        [MaxLength(70)]
        public string? TeamsEmail { get; set; }
        [MaxLength(70)]
        public string? AlternateEmail { get; set; }
        [MaxLength(200)]
        public string? DismissReason { get; set; }
    }
}