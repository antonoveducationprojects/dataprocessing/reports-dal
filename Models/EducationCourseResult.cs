using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;
namespace ReportsDAL.Models
{

    public class EducationCourseResult
    {
        public ulong EducationCourseResultId  { get; set; }
        [Required]
        public ulong EducationCourseId{ get; set; }
        public EducationCourse EducationCourse { get; set; }
    
        [MaxLength(100)]
        [Required]
        public string FullName { get; set; }
    
        [MaxLength(100)]
        [Required]
        public string Email { get; set; }
    
        [MaxLength(100)]
        [Required]
        public string ParticipantGroup { get; set; }
    
        [MaxLength(100)]
        [Required]
        public string BlockName { get; set; }
    
        [MaxLength(100)]
        [Required]
        public string BlockType { get; set; }
    
        public int BlockNestingLevel { get; set; }
        [MaxLength(100)]
        public string Status { get; set; }
        public int? Progress { get; set; }
        public int? Grade { get; set; }
        public int? Score { get; set; }
    
        [MaxLength(250)]
        public string? Mentor { get; set; }
    
        [MaxLength(100)]
        [Required]
        public string? Reviewer { get; set; }
        public DateTime? StartedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public DateTime? CompletedOn { get; set; }
    }
}