using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReportsDAL.Models
{
    public class Submodule
    {
        public ulong SubmoduleId { get; set; }
        public ulong EducationCourseId{ get; set; }
        public EducationCourse EducationCourse { get; set; }

        [MaxLength(100)]
        [Required]
        public string SubmoduleName { get; set; }
        [Required]
        public uint TotalTasksNumber { get; set; }
        [Required]
        public uint MinTaskNumber { get; set; }

        [ForeignKey("SubmoduleId")] 
        public virtual ICollection<TaskMinLevel> TaskMinLevels { get; set; }
    }
}