using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReportsDAL.Models
{
    public class Location
    {
        public ulong LocationId{ get; set; }
        [MaxLength(70)]
        [Required]
        public string LocationName{ get; set; }

        [MaxLength(/*10*/20)]
        [Required]
        public string /*Location*/ShortName{ get; set; }
        
        [ForeignKey("LocationId")]
        public virtual ICollection<Region> Regions { get; set; }
    }
}