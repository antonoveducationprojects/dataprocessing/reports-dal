using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;

namespace ReportsDAL.Models
{
    public class DataSetTable
    {
        public ulong DataSetTableId{ get; set; }
        public ulong EducationGroupId{ get; set; }
        public EducationGroup EducationGroup { get; set; }
        [MaxLength(100)]
        [Required]
        public string FullName { get; set; }
        [MaxLength(70)]
        [Required]
        public string Email { get; set; }
        [MaxLength(100)]
        [Required]
        public string Submodule { get; set; }
        [MaxLength(100)]
        [Required]
        public string TaskName { get; set; }
        public int Score { get; set; } 
        public DateTime ReleaseDate  { get; set; }
    }
}