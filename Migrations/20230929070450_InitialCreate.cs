﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using MySql.EntityFrameworkCore.Metadata;

#nullable disable

namespace ReportsDAL.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CourseStartTypes",
                columns: table => new
                {
                    CourseStartTypeId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StartTypeName = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseStartTypes", x => x.CourseStartTypeId);
                });

            migrationBuilder.CreateTable(
                name: "EducationCourses",
                columns: table => new
                {
                    EducationCourseId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EducationCourseName = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EducationCourses", x => x.EducationCourseId);
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    LocationId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    LocationName = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: false),
                    LocationShortName = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.LocationId);
                });

            migrationBuilder.CreateTable(
                name: "StudentStates",
                columns: table => new
                {
                    StudentStateId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    StateName = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentStates", x => x.StudentStateId);
                });

            migrationBuilder.CreateTable(
                name: "EducationGroups",
                columns: table => new
                {
                    EducationGroupId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EducationGroupName = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: false),
                    CourseStartTypeId = table.Column<ulong>(type: "bigint unsigned", nullable: true),
                    StartedAt = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EducationGroups", x => x.EducationGroupId);
                    table.ForeignKey(
                        name: "FK_EducationGroups_CourseStartTypes_CourseStartTypeId",
                        column: x => x.CourseStartTypeId,
                        principalTable: "CourseStartTypes",
                        principalColumn: "CourseStartTypeId");
                });

            migrationBuilder.CreateTable(
                name: "EducationCourseResults",
                columns: table => new
                {
                    EducationCourseResultId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EducationCourseId = table.Column<ulong>(type: "bigint unsigned", nullable: false),
                    FullName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Email = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    ParticipantGroup = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    BlockName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    BlockType = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    BlockNestingLevel = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Progress = table.Column<int>(type: "int", nullable: true),
                    Grade = table.Column<int>(type: "int", nullable: true),
                    Score = table.Column<int>(type: "int", nullable: true),
                    Mentor = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    Reviewer = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    StartedOn = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedOn = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    CompletedOn = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EducationCourseResults", x => x.EducationCourseResultId);
                    table.ForeignKey(
                        name: "FK_EducationCourseResults_EducationCourses_EducationCourseId",
                        column: x => x.EducationCourseId,
                        principalTable: "EducationCourses",
                        principalColumn: "EducationCourseId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Submodules",
                columns: table => new
                {
                    SubmoduleId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EducationCourseId = table.Column<ulong>(type: "bigint unsigned", nullable: false),
                    SubmoduleName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    TotalTasksNumber = table.Column<uint>(type: "int unsigned", nullable: false),
                    MinTaskNumber = table.Column<uint>(type: "int unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Submodules", x => x.SubmoduleId);
                    table.ForeignKey(
                        name: "FK_Submodules_EducationCourses_EducationCourseId",
                        column: x => x.EducationCourseId,
                        principalTable: "EducationCourses",
                        principalColumn: "EducationCourseId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Regions",
                columns: table => new
                {
                    RegionId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    RegionName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    LocationId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regions", x => x.RegionId);
                    table.ForeignKey(
                        name: "FK_Regions_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DataSetTables",
                columns: table => new
                {
                    DataSetTableId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EducationGroupId = table.Column<ulong>(type: "bigint unsigned", nullable: false),
                    FullName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Email = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: false),
                    Submodule = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    TaskName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Score = table.Column<int>(type: "int", nullable: false),
                    ReleaseDate = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataSetTables", x => x.DataSetTableId);
                    table.ForeignKey(
                        name: "FK_DataSetTables_EducationGroups_EducationGroupId",
                        column: x => x.EducationGroupId,
                        principalTable: "EducationGroups",
                        principalColumn: "EducationGroupId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MeetingSummaries",
                columns: table => new
                {
                    MeetingSummaryId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EducationGroupId = table.Column<ulong>(type: "bigint unsigned", nullable: false),
                    ParticipantsTotalNumber = table.Column<uint>(type: "int unsigned", nullable: false),
                    MeetingTitle = table.Column<string>(type: "varchar(120)", maxLength: 120, nullable: true),
                    MeetingStartTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    MeetingEndTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    MeetingGUID = table.Column<string>(type: "longtext", nullable: true),
                    LectorEmail = table.Column<string>(type: "longtext", nullable: true),
                    LectorId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeetingSummaries", x => x.MeetingSummaryId);
                    table.ForeignKey(
                        name: "FK_MeetingSummaries_EducationGroups_EducationGroupId",
                        column: x => x.EducationGroupId,
                        principalTable: "EducationGroups",
                        principalColumn: "EducationGroupId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaskMinLevels",
                columns: table => new
                {
                    TaskMinLevelId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    TaskName = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: false),
                    Score = table.Column<uint>(type: "int unsigned", nullable: false),
                    Submodule = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    SubmoduleId = table.Column<ulong>(type: "bigint unsigned", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskMinLevels", x => x.TaskMinLevelId);
                    table.ForeignKey(
                        name: "FK_TaskMinLevels_Submodules_SubmoduleId",
                        column: x => x.SubmoduleId,
                        principalTable: "Submodules",
                        principalColumn: "SubmoduleId");
                });

            migrationBuilder.CreateTable(
                name: "Participants",
                columns: table => new
                {
                    ParticipantId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    EducationGroupId = table.Column<ulong>(type: "bigint unsigned", nullable: false),
                    RegionId = table.Column<ulong>(type: "bigint unsigned", nullable: true),
                    RegionName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    GitNick = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: true),
                    FullName = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: false),
                    Email = table.Column<string>(type: "longtext", nullable: true),
                    Active = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    EnglishLevel = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: true),
                    StudentStateId = table.Column<ulong>(type: "bigint unsigned", maxLength: 70, nullable: true),
                    Comments = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    FinalTaskName = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    FinalTaskLink = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    TeamsEmail = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: true),
                    AlternateEmail = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: true),
                    DismissReason = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Participants", x => x.ParticipantId);
                    table.ForeignKey(
                        name: "FK_Participants_EducationGroups_EducationGroupId",
                        column: x => x.EducationGroupId,
                        principalTable: "EducationGroups",
                        principalColumn: "EducationGroupId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Participants_Regions_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Regions",
                        principalColumn: "RegionId");
                    table.ForeignKey(
                        name: "FK_Participants_StudentStates_StudentStateId",
                        column: x => x.StudentStateId,
                        principalTable: "StudentStates",
                        principalColumn: "StudentStateId");
                });

            migrationBuilder.CreateTable(
                name: "MeetingParticipants",
                columns: table => new
                {
                    MeetingParticipantId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    FullName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    JoinTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    LeaveTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    Duration = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    Email = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: false),
                    Role = table.Column<string>(type: "varchar(70)", maxLength: 70, nullable: false),
                    ParticipantGUID = table.Column<string>(type: "longtext", nullable: true),
                    MeetingSummaryId = table.Column<ulong>(type: "bigint unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeetingParticipants", x => x.MeetingParticipantId);
                    table.ForeignKey(
                        name: "FK_MeetingParticipants_MeetingSummaries_MeetingSummaryId",
                        column: x => x.MeetingSummaryId,
                        principalTable: "MeetingSummaries",
                        principalColumn: "MeetingSummaryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DataSetTables_EducationGroupId",
                table: "DataSetTables",
                column: "EducationGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_EducationCourseResults_EducationCourseId",
                table: "EducationCourseResults",
                column: "EducationCourseId");

            migrationBuilder.CreateIndex(
                name: "IX_EducationGroups_CourseStartTypeId",
                table: "EducationGroups",
                column: "CourseStartTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MeetingParticipants_MeetingSummaryId",
                table: "MeetingParticipants",
                column: "MeetingSummaryId");

            migrationBuilder.CreateIndex(
                name: "IX_MeetingSummaries_EducationGroupId",
                table: "MeetingSummaries",
                column: "EducationGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Participants_EducationGroupId",
                table: "Participants",
                column: "EducationGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Participants_RegionId",
                table: "Participants",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_Participants_StudentStateId",
                table: "Participants",
                column: "StudentStateId");

            migrationBuilder.CreateIndex(
                name: "IX_Regions_LocationId",
                table: "Regions",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Submodules_EducationCourseId",
                table: "Submodules",
                column: "EducationCourseId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskMinLevels_SubmoduleId",
                table: "TaskMinLevels",
                column: "SubmoduleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DataSetTables");

            migrationBuilder.DropTable(
                name: "EducationCourseResults");

            migrationBuilder.DropTable(
                name: "MeetingParticipants");

            migrationBuilder.DropTable(
                name: "Participants");

            migrationBuilder.DropTable(
                name: "TaskMinLevels");

            migrationBuilder.DropTable(
                name: "MeetingSummaries");

            migrationBuilder.DropTable(
                name: "Regions");

            migrationBuilder.DropTable(
                name: "StudentStates");

            migrationBuilder.DropTable(
                name: "Submodules");

            migrationBuilder.DropTable(
                name: "EducationGroups");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "EducationCourses");

            migrationBuilder.DropTable(
                name: "CourseStartTypes");
        }
    }
}
