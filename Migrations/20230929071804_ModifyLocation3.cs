﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ReportsDAL.Migrations
{
    public partial class ModifyLocation3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LocationShortName",
                table: "Locations",
                newName: "ShortName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ShortName",
                table: "Locations",
                newName: "LocationShortName");
        }
    }
}
