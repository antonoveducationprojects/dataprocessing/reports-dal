CREATE TABLE IF NOT EXISTS `__EFMigrationsHistory` (
    `MigrationId` varchar(150) NOT NULL,
    `ProductVersion` varchar(32) NOT NULL,
    PRIMARY KEY (`MigrationId`)
);

CREATE TABLE `CourseStartType` (
    `CourseStartTypeId` bigint NOT NULL AUTO_INCREMENT,
    `StartTypeName` varchar(70) NOT NULL,
    PRIMARY KEY (`CourseStartTypeId`)
);

CREATE TABLE `EducationCourses` (
    `EducationCourseId` bigint NOT NULL AUTO_INCREMENT,
    `EducationCourseName` varchar(70) NOT NULL,
    PRIMARY KEY (`EducationCourseId`)
);

CREATE TABLE `Location` (
    `LocationId` bigint NOT NULL AUTO_INCREMENT,
    `LocationName` varchar(70) NOT NULL,
    PRIMARY KEY (`LocationId`)
);

CREATE TABLE `StudentStates` (
    `StudentStateId` bigint NOT NULL AUTO_INCREMENT,
    `StateName` varchar(70) NOT NULL,
    PRIMARY KEY (`StudentStateId`)
);

CREATE TABLE `EducationGroups` (
    `EducationGroupId` bigint NOT NULL AUTO_INCREMENT,
    `EducationGroupName` varchar(70) NOT NULL,
    `CourseStartTypeId` bigint NOT NULL,
    PRIMARY KEY (`EducationGroupId`),
    CONSTRAINT `FK_EducationGroups_CourseStartType_CourseStartTypeId` FOREIGN KEY (`CourseStartTypeId`) REFERENCES `CourseStartType` (`CourseStartTypeId`) ON DELETE CASCADE
);

CREATE TABLE `EducationCourseResults` (
    `EducationCourseResultId` bigint NOT NULL AUTO_INCREMENT,
    `EducationCourseId` bigint NOT NULL,
    `FullName` varchar(100) NOT NULL,
    `Email` varchar(100) NOT NULL,
    `ParticipantGroup` varchar(100) NOT NULL,
    `BlockName` varchar(100) NOT NULL,
    `BlockType` varchar(100) NOT NULL,
    `Status` varchar(100) NULL,
    `Progress` int NULL,
    `Grade` int NULL,
    `Score` int NULL,
    `Mentor` varchar(250) NULL,
    `Reviewer` varchar(100) NOT NULL,
    `StartedOn` datetime NULL,
    `UpdatedOn` datetime NULL,
    `CompletedOn` datetime NULL,
    PRIMARY KEY (`EducationCourseResultId`),
    CONSTRAINT `FK_EducationCourseResults_EducationCourses_EducationCourseId` FOREIGN KEY (`EducationCourseId`) REFERENCES `EducationCourses` (`EducationCourseId`) ON DELETE CASCADE
);

CREATE TABLE `Submodules` (
    `SubmoduleId` bigint NOT NULL AUTO_INCREMENT,
    `EducationCourseId` bigint NOT NULL,
    `SubmoduleName` varchar(100) NOT NULL,
    `TotalTasksNumber` int NOT NULL,
    `MinTaskNumber` int NOT NULL,
    PRIMARY KEY (`SubmoduleId`),
    CONSTRAINT `FK_Submodules_EducationCourses_EducationCourseId` FOREIGN KEY (`EducationCourseId`) REFERENCES `EducationCourses` (`EducationCourseId`) ON DELETE CASCADE
);

CREATE TABLE `Region` (
    `RegionId` bigint NOT NULL AUTO_INCREMENT,
    `RegionName` varchar(100) NOT NULL,
    `LocationId` bigint NOT NULL,
    PRIMARY KEY (`RegionId`),
    CONSTRAINT `FK_Region_Location_LocationId` FOREIGN KEY (`LocationId`) REFERENCES `Location` (`LocationId`) ON DELETE CASCADE
);

CREATE TABLE `DataSetTables` (
    `DataSetTableId` bigint NOT NULL AUTO_INCREMENT,
    `EducationGroupId` bigint NOT NULL,
    `FullName` varchar(100) NOT NULL,
    `Email` varchar(70) NOT NULL,
    `Submodule` varchar(100) NOT NULL,
    `TaskName` varchar(100) NOT NULL,
    `Score` int NOT NULL,
    `ReleaseDate` datetime NOT NULL,
    PRIMARY KEY (`DataSetTableId`),
    CONSTRAINT `FK_DataSetTables_EducationGroups_EducationGroupId` FOREIGN KEY (`EducationGroupId`) REFERENCES `EducationGroups` (`EducationGroupId`) ON DELETE CASCADE
);

CREATE TABLE `MeetingSummaries` (
    `MeetingSummaryId` bigint NOT NULL AUTO_INCREMENT,
    `EducationGroupId` bigint NOT NULL,
    `ParticipantsTotalNumber` int NOT NULL,
    `MeetingTitle` varchar(120) NULL,
    `MeetingStartTime` datetime NOT NULL,
    `MeetingEndTime` datetime NOT NULL,
    `MeetingGUID` text NULL,
    PRIMARY KEY (`MeetingSummaryId`),
    CONSTRAINT `FK_MeetingSummaries_EducationGroups_EducationGroupId` FOREIGN KEY (`EducationGroupId`) REFERENCES `EducationGroups` (`EducationGroupId`) ON DELETE CASCADE
);

CREATE TABLE `TaskMinLevels` (
    `TaskMinLevelId` bigint NOT NULL AUTO_INCREMENT,
    `TaskName` varchar(150) NOT NULL,
    `Score` int NOT NULL,
    `Submodule` varchar(100) NOT NULL,
    `SubmoduleId` bigint NULL,
    PRIMARY KEY (`TaskMinLevelId`),
    CONSTRAINT `FK_TaskMinLevels_Submodules_SubmoduleId` FOREIGN KEY (`SubmoduleId`) REFERENCES `Submodules` (`SubmoduleId`) ON DELETE RESTRICT
);

CREATE TABLE `Participants` (
    `ParticipantId` bigint NOT NULL AUTO_INCREMENT,
    `EducationGroupId` bigint NOT NULL,
    `RegionId` bigint NOT NULL,
    `RegionName` varchar(100) NOT NULL,
    `GitNick` varchar(70) NULL,
    `FullName` varchar(70) NOT NULL,
    `EnglishLevel` varchar(70) NOT NULL,
    `Email` varchar(70) NOT NULL,
    `Active` tinyint(1) NOT NULL,
    `StudentStateId` bigint NOT NULL,
    `Comments` varchar(250) NULL,
    `FinalTaskName` varchar(250) NULL,
    `FinalTaskLink` varchar(250) NULL,
    `TeamsEmail` varchar(70) NULL,
    `AlternateEmail` varchar(70) NULL,
    `DismissReason` varchar(200) NULL,
    PRIMARY KEY (`ParticipantId`),
    CONSTRAINT `FK_Participants_EducationGroups_EducationGroupId` FOREIGN KEY (`EducationGroupId`) REFERENCES `EducationGroups` (`EducationGroupId`) ON DELETE CASCADE,
    CONSTRAINT `FK_Participants_Region_RegionId` FOREIGN KEY (`RegionId`) REFERENCES `Region` (`RegionId`) ON DELETE CASCADE,
    CONSTRAINT `FK_Participants_StudentStates_StudentStateId` FOREIGN KEY (`StudentStateId`) REFERENCES `StudentStates` (`StudentStateId`) ON DELETE CASCADE
);

CREATE TABLE `MeetingParticipants` (
    `MeetingParticipantId` bigint NOT NULL AUTO_INCREMENT,
    `FullName` varchar(100) NOT NULL,
    `JoinTime` datetime NOT NULL,
    `LeaveTime` datetime NOT NULL,
    `Duration` datetime NOT NULL,
    `Email` varchar(70) NOT NULL,
    `Role` varchar(70) NOT NULL,
    `ParticipantGUID` text NULL,
    `MeetingSummaryId` bigint NOT NULL,
    PRIMARY KEY (`MeetingParticipantId`),
    CONSTRAINT `FK_MeetingParticipants_MeetingSummaries_MeetingSummaryId` FOREIGN KEY (`MeetingSummaryId`) REFERENCES `MeetingSummaries` (`MeetingSummaryId`) ON DELETE CASCADE
);

CREATE INDEX `IX_DataSetTables_EducationGroupId` ON `DataSetTables` (`EducationGroupId`);

CREATE INDEX `IX_EducationCourseResults_EducationCourseId` ON `EducationCourseResults` (`EducationCourseId`);

CREATE INDEX `IX_EducationGroups_CourseStartTypeId` ON `EducationGroups` (`CourseStartTypeId`);

CREATE INDEX `IX_MeetingParticipants_MeetingSummaryId` ON `MeetingParticipants` (`MeetingSummaryId`);

CREATE INDEX `IX_MeetingSummaries_EducationGroupId` ON `MeetingSummaries` (`EducationGroupId`);

CREATE INDEX `IX_Participants_EducationGroupId` ON `Participants` (`EducationGroupId`);

CREATE INDEX `IX_Participants_RegionId` ON `Participants` (`RegionId`);

CREATE INDEX `IX_Participants_StudentStateId` ON `Participants` (`StudentStateId`);

CREATE INDEX `IX_Region_LocationId` ON `Region` (`LocationId`);

CREATE INDEX `IX_Submodules_EducationCourseId` ON `Submodules` (`EducationCourseId`);

CREATE INDEX `IX_TaskMinLevels_SubmoduleId` ON `TaskMinLevels` (`SubmoduleId`);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20221017203719_InitialCommit', '3.1.10');


